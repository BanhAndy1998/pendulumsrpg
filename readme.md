A turn-based RPG created for CS583. There are three only three battles in this RPG, but some strategy is required to clear normal and planning is required to clear hard.

Instructions:

Use the WASD keys to move around on the Overworld. C keys to pick up items that Stark is ontop of.

While in battle, Only the mouse is used.

Game ends when you defeat the final enemy, Lionel, in battle.

ALL DIFFICUTIES ARE CLEARABLE WITHOUT DYING. THERE IS ANOTHER DOCUMENT I INCLUDED ON HOW TO CLEAR HARD WITHOUT DYING. THIS WORKS FOR NORMAL AS WELL.


**ASSETS LIST**

Character Sprites for Stark, Kyra, Vayne, and Lionel were created and all taken from
https://sanderfrenken.github.io/Universal-LPC-Spritesheet-Character-Generator/

Dungeon Tiles Tileset was taken from 
https://safwyl.itch.io/

Item Tiles was taken from
https://wilfryed.itch.io/

Door Tile was taken from
https://opengameart.org/content/rpg-tileset-32x32

All of the graphic text and buttons were created by me using GIMP



**SCRIPTS**

All scripts were written by me. However, small snippets of code in Script_Overworld and Script_Battle were referenced
from various StackOverflow and Unity Manual pages



**MUSIC AND SFX**

MAIN MENU - Falling The Star Light ~Waltz~ (From ATELIER RORONA), Spliced using Audacity
https://www.youtube.com/watch?v=dQBSvGRGdrw

OVERWORLD - Kronos (SELENTIA ALBUM by SAKUZYO)
https://www.youtube.com/watch?v=8noVKITOC68

BATTLE WITH KYRA AND VAYNE - 翠雨の祷 (From SOUND VOLTEX VIVID WAVE by CORORO feat YURIA MIYAZONO)
https://www.youtube.com/watch?v=-JlhX46IcMA

BATTLE WITH LIONEL - Lachryma《Re:Queen’M》 (From SOUND VOLTEX III: GRAVITY WARS by KANEKO CHIHARU)
https://www.youtube.com/watch?v=mAGMnmKDAHs

CREDITS - I (From SOUND VOLTEX IV: HEAVENLY HAVEN by KUROMA)
https://www.youtube.com/watch?v=2z7ScMOIBWg

SLASH SFX - By SOUNDCHIPS, Spliced using Audacity
https://www.youtube.com/watch?v=0_OIkmarZek

ARROW SFX - By CHRIS BALLENGEE, Spliced using Audacity
https://www.youtube.com/watch?v=0mXqm71fGBQ
