﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class StarkController : MonoBehaviour
{
    public GameObject Stark;
    public Animator anim;
    public Tilemap Wall;
    public int movingDirection = 0; //1 North, 2 West, 3 South, 4 East
    bool delay = false; 
    public bool moving = false;
    public static bool movingEnabled = true;

    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetFloat("VMove", 0);
        anim.SetFloat("HMove", 0);
    }

    void Update()
    {

        if (Input.GetKey("w") && (movingDirection == 0 || movingDirection == 1) && !moving && movingEnabled )
        {
            moving = true; 
            anim.SetFloat("VMove", 1);
            movingDirection = 1;
            StartCoroutine(MoveN());

        }
        if (Input.GetKey("a") && (movingDirection == 0 || movingDirection == 2) && !moving && movingEnabled)
        {
            moving = true;
            anim.SetFloat("HMove", -1);
            movingDirection = 2;
            StartCoroutine(MoveW());
        }
        if (Input.GetKey("s") && (movingDirection == 0 || movingDirection == 3) && !moving && movingEnabled)
        {
            moving = true;
            anim.SetFloat("VMove", -1);
            movingDirection = 3;
            StartCoroutine(MoveS());
        }
        if (Input.GetKey("d") && (movingDirection == 0 || movingDirection == 4) && !moving && movingEnabled)
        {
            moving = true;
            anim.SetFloat("HMove", 1);
            movingDirection = 4;
            StartCoroutine(MoveE());
        }

    }

    public IEnumerator MoveN()
    {
        if (Wall.GetTile(new Vector3Int((int)System.Math.Ceiling(Stark.transform.position.x-2), (int)System.Math.Floor(Stark.transform.position.y+1), 0)) != null)
        {
            invalidMoveN();
            yield break;
        }
        Vector2 target = Stark.transform.position;
        target.y += 1;
        Vector2 smoothMove = Stark.transform.position;
        while (Stark.transform.position.y - target.y < -.01 || Stark.transform.position.y - target.y > .01)
        {
            smoothMove.y += 0.1F;
            Stark.transform.position = Vector2.MoveTowards(Stark.transform.position, smoothMove, 0.1F);
            yield return new WaitForSeconds(.015f);
        }
        anim.SetFloat("VMove", .1f);
        anim.SetFloat("HMove", 0);
        RoundPosition();
    }

    public IEnumerator MoveW()
    {
        if (Wall.GetTile(new Vector3Int((int)System.Math.Floor(Stark.transform.position.x-2), (int)System.Math.Floor(Stark.transform.position.y), 0)) != null)
        {
            invalidMoveW();
            yield break;
        }
        Vector2 target = Stark.transform.position;
        target.x -= 1;
        Vector2 smoothMove = Stark.transform.position;
        while (Stark.transform.position.x - target.x < -.01 || Stark.transform.position.x - target.x > .01)
        {
            smoothMove.x -= 0.1F;
            Stark.transform.position = Vector2.MoveTowards(Stark.transform.position, smoothMove, 0.1F);
            yield return new WaitForSeconds(.015f);
        }
        anim.SetFloat("HMove", -.1f);
        anim.SetFloat("VMove", 0);
        RoundPosition();
    }

    public IEnumerator MoveS()
    {
        if (Wall.GetTile(new Vector3Int((int)System.Math.Ceiling(Stark.transform.position.x-2), (int)System.Math.Floor(Stark.transform.position.y-1), 0)) != null)
        {
            invalidMoveS();
            yield break;
        }
        Vector2 target = Stark.transform.position;
        target.y -= 1;
        Vector2 smoothMove = Stark.transform.position;
        while (Stark.transform.position.y - target.y < -.01 || Stark.transform.position.y - target.y > .01)
        {
            smoothMove.y -= 0.1F;
            Stark.transform.position = Vector2.MoveTowards(Stark.transform.position, smoothMove, 0.1F);
            yield return new WaitForSeconds(.015f);
        }
        anim.SetFloat("VMove", -.1f);
        anim.SetFloat("HMove", 0);
        RoundPosition();
    }

    public IEnumerator MoveE()
    {
        if (Wall.GetTile(new Vector3Int((int)System.Math.Ceiling(Stark.transform.position.x - 1), (int)System.Math.Floor(Stark.transform.position.y), 0)) != null) { 
            invalidMoveE();
            yield break;
        }
        Vector2 target = Stark.transform.position;
        target.x += 1;
        Vector2 smoothMove = Stark.transform.position;
        while (Stark.transform.position.x - target.x < -.01 || Stark.transform.position.x - target.x > .01)
        {
            smoothMove.x += 0.1F;
            Stark.transform.position = Vector2.MoveTowards(Stark.transform.position, smoothMove, 0.1F);
            yield return new WaitForSeconds(.015f);
        }
        anim.SetFloat("HMove", .1f);
        anim.SetFloat("VMove", 0); 
        RoundPosition();
    }

    public void RoundPosition()
    {
        movingDirection = 0;
        double toRoundX = Stark.transform.position.x;
        double toRoundY = Stark.transform.position.y;
        toRoundX = System.Math.Round(toRoundX, 1);
        toRoundY = System.Math.Round(toRoundY, 1);
        transform.position = new Vector2((float)toRoundX, (float)toRoundY);
        moving = false;
    }

    public void invalidMoveN()
    {
        anim.SetFloat("VMove", .1f);
        anim.SetFloat("HMove", 0);
        movingDirection = 0;
        moving = false;
    }

    public void invalidMoveW()
    {
        anim.SetFloat("HMove", -.1f);
        anim.SetFloat("VMove", 0);
        movingDirection = 0;
        moving = false;
    }

    public void invalidMoveS()
    {
        anim.SetFloat("VMove", -.1f);
        anim.SetFloat("HMove", 0);
        movingDirection = 0;
        moving = false;
    }

    public void invalidMoveE()
    {
        anim.SetFloat("HMove", .1f);
        anim.SetFloat("VMove", 0);
        movingDirection = 0;
        moving = false;
    }

    public static void DisableMovement()
    {
        movingEnabled = false;
    }

    public static void EnableMovement()
    {
        movingEnabled = true;
    }
}
