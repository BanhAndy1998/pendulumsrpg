﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    private static PlayerInfo playerInfo = null;
    public int difficulty = 0;
    public Character stark;
    public Character kyra;
    public Character vayne;
    public Character lionel;
    public bool continued = false;
    public int progress = 0; //1 After 1st win, 2 After 2nd
    public List<string> collectedItems = new List<string>();
    public List<string> inventory = new List<string>();

    public static PlayerInfo Instance
    {
        get { return playerInfo; }
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this);
            playerInfo = this;
        }
        else
        {
            if (playerInfo != this)
                Destroy(gameObject);
        }
    }   

    public static void DestroyThis()
    {
        playerInfo = null;
    }
}
