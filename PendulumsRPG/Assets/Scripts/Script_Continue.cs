﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_Continue : MonoBehaviour
{
    public Button Button_Yes;
    public Button Button_No;

    public void continueGame()
    {
        PlayerInfo.Instance.continued = true;
        PlayerInfo.Instance.collectedItems.Clear();
        SceneManager.LoadScene("Ingame");
    }

    public void returnToMainMenu()
    {
        PlayerInfo.DestroyThis();
        SceneManager.LoadScene("Main Menu");
    }
}