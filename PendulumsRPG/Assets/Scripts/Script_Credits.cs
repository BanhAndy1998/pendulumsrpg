﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_Credits : MonoBehaviour
{
    public Button Button_Next;

    public void exitCredits()
    {
        SceneManager.LoadScene("Continue");
    }
}
