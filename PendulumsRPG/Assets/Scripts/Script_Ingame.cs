﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_Ingame : MonoBehaviour
{
    public GameObject StarkOverworld;
    public GameObject KyraOverworld;
    public GameObject VayneOverworld;
    public GameObject LionelOverworld;
    public StarkController Controller;
    public Animator Music;
    public Text HpVal;
    public Button UseItem;
    public Dropdown usables;

    void Awake()
    {

        StarkController.EnableMovement();

        if (PlayerInfo.Instance.progress == 0 && !PlayerInfo.Instance.continued)
        {
            PlayerInfo.Instance.stark = new Character(1, 30, 10, 8);   
        }

        if (PlayerInfo.Instance.difficulty == 1)
        {
            PlayerInfo.Instance.kyra = new Character(1, 30, 6, 6);
            PlayerInfo.Instance.vayne = new Character(1, 100, 12, 9);
            PlayerInfo.Instance.lionel = new Character(1, 350, 60, 12);
        }
        if (PlayerInfo.Instance.difficulty == 2)
        {
            PlayerInfo.Instance.kyra = new Character(0, 60, 10, 6);
            PlayerInfo.Instance.vayne = new Character(0, 140, 16, 12);
            PlayerInfo.Instance.lionel = new Character(0, 600, 100, 21);
        }
        if (PlayerInfo.Instance.difficulty == 3)
        {
            PlayerInfo.Instance.kyra = new Character(0, 70, 10, 6);
            PlayerInfo.Instance.vayne = new Character(0, 160, 21, 12);
            PlayerInfo.Instance.lionel = new Character(0, 1000, 200, 27);
        }

        HpVal.text = PlayerInfo.Instance.stark.hp + "/" + PlayerInfo.Instance.stark.maxHp;

        if (PlayerInfo.Instance.inventory.Count != 0) { 
            foreach (string x in PlayerInfo.Instance.inventory)
            {
                if(x == "Health Potion" || x == "Super Potion")
                    usables.options.Add(new Dropdown.OptionData() { text = x });
            }
        }
        if(PlayerInfo.Instance.progress == 1)
        {
            KyraOverworld.SetActive(false);
            StarkOverworld.transform.position = new Vector3((float)-0.5, (float)20.5, 0);
            DeleteCollected();
        }
        if (PlayerInfo.Instance.progress == 2)
        {
            KyraOverworld.SetActive(false);
            VayneOverworld.SetActive(false);
            StarkOverworld.transform.position = new Vector3((float)-0.5, (float)40.5, 0);
            DeleteCollected();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("c"))
        {
            StartCoroutine(ItemCollect());
        }
        if (StarkOverworld.transform.position.x == -0.5 && StarkOverworld.transform.position.y == 20.5 && PlayerInfo.Instance.progress == 0)
        {
            StartCoroutine(BeginBattle());
        }
        if (StarkOverworld.transform.position.x == -0.5 && StarkOverworld.transform.position.y == 40.5 && PlayerInfo.Instance.progress == 1)
        {
            StartCoroutine(BeginBattle());
        }
        if (StarkOverworld.transform.position.x == -0.5 && StarkOverworld.transform.position.y == 70.5 && PlayerInfo.Instance.progress == 2)
        {
            StartCoroutine(BeginBattle());
        }
        if (usables.options.Count == 0 || PlayerInfo.Instance.stark.hp == PlayerInfo.Instance.stark.maxHp)
            UseItem.interactable = false;
        else
            UseItem.interactable = true;
    }
    
    public void DeleteCollected()
    {
        GameObject item;
        if(PlayerInfo.Instance.collectedItems.Contains("HPPotion1")) {
            item = GameObject.Find("HPPotion1");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("ChronosPotion1")) {
            item = GameObject.Find("ChronosPotion1");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("AttackBooster")) {
            item = GameObject.Find("AttackBooster");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("HPPotion2")) {
            item = GameObject.Find("HPPotion2");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("MagicalRuneGodspeed")) {
            item = GameObject.Find("MagicalRuneGodspeed");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("HPPotion3")) {
            item = GameObject.Find("HPPotion3");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("ChronosPotion2")) {
            item = GameObject.Find("ChronosPotion2");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("TecBooster")) {
            item = GameObject.Find("TecBooster");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("MagicalRuneBomb")) {
            item = GameObject.Find("MagicalRuneBomb");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("MagicalRuneBladesdance")) {
            item = GameObject.Find("MagicalRuneBladesdance");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("HPPotion4")) {
            item = GameObject.Find("HPPotion4");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("HPPotion5"))
        {
            item = GameObject.Find("HPPotion5");
            item.SetActive(false);
        }
        if (PlayerInfo.Instance.collectedItems.Contains("SuperChronosPotion"))
        {
            item = GameObject.Find("SuperChronosPotion");
            item.SetActive(false);
        }
    }

    public IEnumerator ItemCollect() {
        GameObject item;

        if (StarkOverworld.transform.position.x == 6.5 && StarkOverworld.transform.position.y == 4.5 && !PlayerInfo.Instance.collectedItems.Contains("HPPotion1"))
        {
            PlayerInfo.Instance.inventory.Add("Health Potion");
            usables.options.Add(new Dropdown.OptionData() { text = "Health Potion" });
            PlayerInfo.Instance.collectedItems.Add("HPPotion1");
            item = GameObject.Find("HPPotion1");
            item.SetActive(false);
            usables.value = 0;
        }
        if (StarkOverworld.transform.position.x == 12.5 && StarkOverworld.transform.position.y == 1.5 && !PlayerInfo.Instance.collectedItems.Contains("ChronosPotion1"))
        {
            PlayerInfo.Instance.inventory.Add("Chronos Potion");
            PlayerInfo.Instance.collectedItems.Add("ChronosPotion1");
            item = GameObject.Find("ChronosPotion1");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -12.5 && StarkOverworld.transform.position.y == 1.5 && !PlayerInfo.Instance.collectedItems.Contains("AttackBooster"))
        {
            PlayerInfo.Instance.inventory.Add("Attack Booster");
            PlayerInfo.Instance.collectedItems.Add("AttackBooster");
            item = GameObject.Find("AttackBooster");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -9.5 && StarkOverworld.transform.position.y == -5.5 && !PlayerInfo.Instance.collectedItems.Contains("HPPotion2"))
        {
            PlayerInfo.Instance.inventory.Add("Health Potion");
            usables.options.Add(new Dropdown.OptionData() { text = "Health Potion" });
            PlayerInfo.Instance.collectedItems.Add("HPPotion2");
            item = GameObject.Find("HPPotion2");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -0.5 && StarkOverworld.transform.position.y == 12.5 && !PlayerInfo.Instance.collectedItems.Contains("MagicalRuneGodspeed"))
        {
            PlayerInfo.Instance.inventory.Add("Rune: Godspeed");
            PlayerInfo.Instance.collectedItems.Add("MagicalRuneGodspeed");
            item = GameObject.Find("MagicalRuneGodspeed");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -3.5 && StarkOverworld.transform.position.y == 26.5 && !PlayerInfo.Instance.collectedItems.Contains("HPPotion3"))
        {
            PlayerInfo.Instance.inventory.Add("Super Potion");
            usables.options.Add(new Dropdown.OptionData() { text = "Super Potion" });
            PlayerInfo.Instance.collectedItems.Add("HPPotion3");
            item = GameObject.Find("HPPotion3");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == 2.5 && StarkOverworld.transform.position.y == 26.5 && !PlayerInfo.Instance.collectedItems.Contains("ChronosPotion2"))
        {
            PlayerInfo.Instance.inventory.Add("Chronos Potion");
            PlayerInfo.Instance.collectedItems.Add("Chronos Potion");
            item = GameObject.Find("ChronosPotion2");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == 0.5 && StarkOverworld.transform.position.y == 28.5 && !PlayerInfo.Instance.collectedItems.Contains("TecBooster"))
        {
            PlayerInfo.Instance.inventory.Add("Tec Booster");
            PlayerInfo.Instance.collectedItems.Add("TecBooster");
            item = GameObject.Find("TecBooster");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -0.5 && StarkOverworld.transform.position.y == 30.5 && !PlayerInfo.Instance.collectedItems.Contains("MagicalRuneExplosive"))
        {
            PlayerInfo.Instance.inventory.Add("Rune: Explosive Stab");
            PlayerInfo.Instance.collectedItems.Add("MagicalRuneExplosive");
            item = GameObject.Find("MagicalRuneExplosive");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -2.5 && StarkOverworld.transform.position.y == 46.5 && !PlayerInfo.Instance.collectedItems.Contains("MagicalRuneBladesdance"))
        {
            PlayerInfo.Instance.inventory.Add("Rune: Bladesdance");
            PlayerInfo.Instance.collectedItems.Add("MagicalRuneBladesdance");
            item = GameObject.Find("MagicalRuneBladesdance");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == 3.5 && StarkOverworld.transform.position.y == 49.5 && !PlayerInfo.Instance.collectedItems.Contains("HPPotion4"))
        {
            PlayerInfo.Instance.inventory.Add("Super Potion");
            usables.options.Add(new Dropdown.OptionData() { text = "Super Potion"});
            PlayerInfo.Instance.collectedItems.Add("HPPotion4");
            item = GameObject.Find("HPPotion4");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == -6.5 && StarkOverworld.transform.position.y == 57.5 && !PlayerInfo.Instance.collectedItems.Contains("HPPotion5"))
        {
            PlayerInfo.Instance.inventory.Add("Super Potion");
            usables.options.Add(new Dropdown.OptionData() { text = "Super Potion" });
            PlayerInfo.Instance.collectedItems.Add("HPPotion5");
            item = GameObject.Find("HPPotion5");
            item.SetActive(false);
        }
        if (StarkOverworld.transform.position.x == 9.5 && StarkOverworld.transform.position.y == 64.5 && !PlayerInfo.Instance.collectedItems.Contains("SuperChronosPotion"))
        {
            PlayerInfo.Instance.inventory.Add("Super Chronos Potion");
            PlayerInfo.Instance.collectedItems.Add("SuperChronosPotion");
            item = GameObject.Find("SuperChronosPotion");
            item.SetActive(false);
        }
        usables.RefreshShownValue();
        yield return 0;
    }

    public IEnumerator BeginBattle()
    {
        StarkController.DisableMovement();
        Music.SetTrigger("QuickAudioFade");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Battle");
    }

    public void UseInvItem()
    {   
        if(usables.options[usables.value].text == "Health Potion")
        {
            if ((int)System.Math.Ceiling(PlayerInfo.Instance.stark.maxHp * .25) + PlayerInfo.Instance.stark.hp > PlayerInfo.Instance.stark.maxHp)
                PlayerInfo.Instance.stark.hp = PlayerInfo.Instance.stark.maxHp;
            else
                PlayerInfo.Instance.stark.hp += (int)System.Math.Ceiling(PlayerInfo.Instance.stark.maxHp * .25);
            PlayerInfo.Instance.inventory.Remove(usables.options[usables.value].text);
            usables.options.RemoveAt(usables.value);
            HpVal.text = PlayerInfo.Instance.stark.hp + "/" + PlayerInfo.Instance.stark.maxHp;
        }
        else if (usables.options[usables.value].text == "Super Potion")
        {
            if ((int)System.Math.Ceiling(PlayerInfo.Instance.stark.maxHp * .5) + PlayerInfo.Instance.stark.hp > PlayerInfo.Instance.stark.maxHp)
                PlayerInfo.Instance.stark.hp = PlayerInfo.Instance.stark.maxHp;
            else
                PlayerInfo.Instance.stark.hp += (int)System.Math.Ceiling(PlayerInfo.Instance.stark.maxHp * .5);
            PlayerInfo.Instance.inventory.Remove(usables.options[usables.value].text);
            usables.options.RemoveAt(usables.value);
            HpVal.text = PlayerInfo.Instance.stark.hp + "/" + PlayerInfo.Instance.stark.maxHp;
        }
        usables.RefreshShownValue();
    }

    public void ExitToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
        PlayerInfo.DestroyThis();
    }

}

