﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Script_Instructions : MonoBehaviour
{
    public Button Button_Next;
    public Button Button_NextGrader;
    public Text Text_Synopsis;
    public Text Text_Grader;
    public Text Text_Instructions;
    public GameObject[] set1;
    public GameObject[] set2;
    int status = 0;

    void Awake()
    {
        set1 = GameObject.FindGameObjectsWithTag("MenuSet1");
        set2 = GameObject.FindGameObjectsWithTag("MenuSet2");
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (status == 1)
        {
            foreach (GameObject x in set1)
                x.SetActive(false);
            foreach (GameObject x in set2)
                x.SetActive(true);
        }
        if (status == 2)
        {
            SceneManager.LoadScene("Ingame");
            StartCoroutine(setVisible());
        } 
    }

    public void graderPress()
    {
        PlayerInfo.Instance.inventory.Add("Instant Kill");
        PlayerInfo.Instance.inventory.Add("Instant Kill");
        PlayerInfo.Instance.inventory.Add("Instant Kill");
        nextPress();
    }

    public void nextPress()
    {
        status += 1;
    }

    public IEnumerator setVisible()
    {
        yield return new WaitForSeconds(.5f);
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
    }
}
