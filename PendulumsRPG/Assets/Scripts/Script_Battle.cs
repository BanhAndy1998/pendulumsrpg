﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Script_Battle : MonoBehaviour
{
    System.Random rand = new System.Random();
    Character Stark;
    Character Enemy;
    public Animator StarkAnim;
    public Animator EnemyAnim;
    public GameObject Kyra;
    public GameObject Vayne;
    public GameObject Lionel;
    public Text StarkHP;
    public Text CPText;
    public Text CPGain;
    public Text EnemyHP;
    public Text StarkDamageTaken;
    public Text InfoBox;
    public Text EnemyDamageTaken;
    public Button Attack;
    public Button PowerStrike;
    public Button DebilitatingSlash;
    public Button Godspeed;
    public Button Bladesdance;
    public Button Guard;
    public Button UseItem;
    public Dropdown ItemList;
    public Button[] UserInput;
    public AudioSource SlashSound;
    public AudioSource SlashSound2;
    public AudioSource SlashSound3;
    public AudioSource SlashSound4;
    public AudioSource SlashSound5;
    public AudioSource SlashSound6;
    public AudioSource SlashSound7;
    public AudioSource BowSound;
    public AudioSource LionelBattle;
    public AudioSource NormalBattle;
    int cp = 30; //Used for skills
    int action; //1 for attack, 2 for Skill1, 3 for Skill2, 4 for skill3, 5 for Super, 6 for item; 7 for guard;
    int target; //1 for Stark, 2 for enemy;
    bool isAnimation = false;
    bool playerTurn = false;
    bool debilitated = false;
    int atkBoosted = 0;
    bool tecBoosted = false;
    int debilitatedTurns = 0;
    int lionelCounter = 0;
    bool guarding = false;
    bool endFight = false;

    void Awake()
    {
        Stark = PlayerInfo.Instance.stark;
        if (PlayerInfo.Instance.progress == 0)
        {
            Kyra.SetActive(true);
            Vayne.SetActive(false);
            Lionel.SetActive(false);
            Enemy = PlayerInfo.Instance.kyra;
            EnemyAnim.SetTrigger("Kyra Initiate");
        }
        if (PlayerInfo.Instance.progress == 1)
        {
            Kyra.SetActive(false);
            Vayne.SetActive(true);
            Lionel.SetActive(false);
            Enemy = PlayerInfo.Instance.vayne;
            EnemyAnim.SetTrigger("Vayne Initiate");
        }
        if (PlayerInfo.Instance.progress == 2)
        {
            Kyra.SetActive(false);
            Vayne.SetActive(false);
            Lionel.SetActive(true);
            Enemy = PlayerInfo.Instance.lionel;
            EnemyAnim.SetTrigger("Lionel Initiate");
            NormalBattle.Stop();
            LionelBattle.Play();
        }

        foreach (Button x in UserInput)
            x.interactable = false;

        StarkDamageTaken.text = "";
        StarkHP.text = Stark.hp + "/" + Stark.maxHp;
        EnemyDamageTaken.text = "";
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        CPGain.text = "";
        CPText.text = cp.ToString();
        InfoBox.text = "";

        foreach (string x in PlayerInfo.Instance.inventory)
        {
            ItemList.options.Add(new Dropdown.OptionData() { text = x });
        }

        showUserUI();

        playerTurn = true;
        action = 0;
    }

    void Update()
    {
        if ( (Stark.hp < 1 || Enemy.hp < 1) && !endFight)
        {
            StartCoroutine(endBattle());
            endFight = true;
        }
            
        if (playerTurn && !isAnimation && !endFight)
        {
            if (action == 1)
            {
                disableUserUI();
                StartCoroutine(starkAttack());
                playerTurn = false;
                isAnimation = true;

            }
            if (action == 2 && cp > 19) //Power Strike, Deals extra damage
            {
                disableUserUI();
                StartCoroutine(starkSkill1());
                StartCoroutine(StarkSkillAudio());
                playerTurn = false;
                isAnimation = true;

            }
            if (action == 3 && cp > 49) //Debilitating Slash, Lowers enemy atk
            {
                disableUserUI();
                StartCoroutine(starkSkill2());
                StartCoroutine(StarkSkillAudio());
                playerTurn = false;
                isAnimation = true;

            }
            if (action == 4 && cp > 69) //Godspeed, Deals extra damage and act again
            {
                StartCoroutine(starkSkillGS());
                StartCoroutine(StarkSkillGSAudio());
                isAnimation = true;
                disableUserUI();
            }
            if (action == 5 && cp > 99) //Bladesdance, Deals heavy damage and ignore defense
            {
                disableUserUI();
                StartCoroutine(starkSkillSuper());
                StartCoroutine(StarkSkillSuperAudio());
                playerTurn = false;
                isAnimation = true;

            }
            if (action == 6)
            {
                disableUserUI();
                StartCoroutine(starkUseItem());
                playerTurn = false;
                isAnimation = true;

            }
            if (action == 7)
            {
                disableUserUI();
                StartCoroutine(starkGuard());
                guarding = true;
                playerTurn = false;
                isAnimation = true;
            }
        }
        if (!playerTurn && !isAnimation && !endFight)
        {
            StartCoroutine(enemyAttack());
            playerTurn = true;
            isAnimation = true;
            //enemyAttack(enemy.getDamage() + rand.Next(0, enemy.getDamage() / 2));   
        }
    }

    public void attackPress()
    {
        action = 1;
    }

    public void skill1Press()
    {
        action = 2;
    }

    public void skill2Press()
    {
        action = 3;
    }

    public void skillGSPress()
    {
        action = 4;
    }

    public void skillSuperPress()
    {
        action = 5;
    }

    public void useItem()
    {
        action = 6;
    }

    public void guard()
    {
        action = 7;
    }

    public IEnumerator starkAttack()
    {
        int totaldamage = Stark.getDamage() + rand.Next(0, Stark.getDamage() / 2) - Enemy.getDR();
        InfoBox.text = "Stark deals " + totaldamage + " damage!";
        if (atkBoosted > 0)
            totaldamage *= 2;
        EnemyDamageTaken.text = "-" + totaldamage;
        Enemy.hp -= totaldamage;
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        cp += 20;
        CPGain.text = "+20";
        CPText.text = cp.ToString();
        StarkAnim.SetTrigger("Attack");
        action = 0;
        SlashSound.PlayDelayed(.3f);
        yield return new WaitForSeconds(1);
        isAnimation = false;
    }

    public IEnumerator starkSkill1() //Power Strike
    {
        int totaldamage = Stark.getDamage() + rand.Next(0, Stark.getDamage() / 2) + (Stark.getDamage() / 2) - Enemy.getDR();
        if (atkBoosted > 0)
            totaldamage *= 2;
        cp -= 20;
        CPGain.text = "-20";
        CPText.text = cp.ToString();
        InfoBox.text = "Stark slashes hard for " + totaldamage + " damage!";
        EnemyDamageTaken.text = "-" + totaldamage;
        Enemy.hp -= totaldamage;
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        StarkAnim.SetTrigger("Skill");
        action = 0;
        yield return new WaitForSeconds(2);
        isAnimation = false;
    }

    public IEnumerator starkSkill2() //Debilitating Slash, Lowers enemy atk
    {
        int totaldamage = Stark.getDamage() + rand.Next(0, Stark.getDamage() / 2) - Enemy.getDR();
        if (atkBoosted > 0)
            totaldamage *= 2;
        cp -= 50;
        CPGain.text = "-50";
        CPText.text = cp.ToString();
        InfoBox.text = "Stark slashes at a vital point for " + totaldamage + " damage! Enemy ATK lowered.";
        EnemyDamageTaken.text = "-" + totaldamage;
        Enemy.hp -= totaldamage;
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        debilitated = true;
        StarkAnim.SetTrigger("Skill");
        action = 0;
        yield return new WaitForSeconds(2);
        isAnimation = false;
    }

    public IEnumerator starkSkillGS() //Godspeed, Deals extra damage and act again
    {
        int totaldamage = Stark.getDamage() * 2 - Enemy.getDR();
        if (atkBoosted > 0)
            totaldamage *= 2;
        cp -= 70;
        CPGain.text = "-70";
        CPText.text = cp.ToString();
        InfoBox.text = "Stark slashes extremely quickly for " + totaldamage + " damage! Act again.";
        EnemyDamageTaken.text = "-" + totaldamage;
        Enemy.hp -= totaldamage;
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        StarkAnim.SetTrigger("SkillGS");
        action = 0;
        yield return new WaitForSeconds(1);
        showUserUI();
        isAnimation = false;
    }

    public IEnumerator starkSkillSuper()
    {
        int totaldamage = Stark.getDamage() * 5;
        if (atkBoosted > 0)
            totaldamage *= 2;
        cp -= 100;
        CPGain.text = "-100";
        CPText.text = cp.ToString();
        InfoBox.text = "Stark turns into a flash of steel and hits for " + totaldamage + " damage!";
        EnemyDamageTaken.text = "-" + totaldamage;
        Enemy.hp -= totaldamage;
        EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
        StarkAnim.SetTrigger("SkillSuper");
        action = 0;
        yield return new WaitForSeconds(2);
        isAnimation = false;
    }

    public IEnumerator starkUseItem()
    {
        if (ItemList.options[ItemList.value].text == "Health Potion")
        {
            CPGain.text = "+ 20";
            cp += 20;
            CPText.text = cp.ToString();
            if ((int)System.Math.Ceiling(Stark.maxHp * .25) + Stark.hp > Stark.maxHp)
                Stark.hp = Stark.maxHp;
            else
                Stark.hp += (int)System.Math.Ceiling(Stark.maxHp * .25);
            StarkHP.text = Stark.hp + "/" + Stark.maxHp;
            StarkDamageTaken.text = "+" + System.Math.Ceiling(Stark.maxHp * .25);
            InfoBox.text = "Stark healed with a Health Potion";
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Super Potion")
        {
            CPGain.text = "+ 20";
            cp += 20;
            CPText.text = cp.ToString();
            if ((int)System.Math.Ceiling(Stark.maxHp * .5) + Stark.hp > Stark.maxHp)
                Stark.hp = Stark.maxHp;
            else
                Stark.hp += (int)System.Math.Ceiling(Stark.maxHp * .5);
            StarkHP.text = Stark.hp + "/" + Stark.maxHp;
            StarkDamageTaken.text = "+" + System.Math.Ceiling(Stark.maxHp * .50);
            InfoBox.text = "Stark healed with a Super Potion";
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Chronos Potion")
        {
            cp += 50;
            CPGain.text = "+ 50";
            CPText.text = cp.ToString();
            InfoBox.text = "Stark restored CP with a Chronos Potion";
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Super Chronos Potion")
        {
            cp += 150;
            CPGain.text = "+ 150";
            CPText.text = cp.ToString();
            InfoBox.text = "Stark restored CP with a Super Chronos Potion";
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Attack Booster")
        {
            CPGain.text = "+ 20";
            cp += 20;
            CPText.text = cp.ToString();
            InfoBox.text = "Stark boosted his own strength";
            atkBoosted = 2;
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Tec Booster")
        {
            CPGain.text = "+ 20";
            cp += 20;
            CPText.text = cp.ToString();
            InfoBox.text = "Stark boosted his own defense";
            tecBoosted = true;
            StarkAnim.SetTrigger("Item");
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text == "Rune: Godspeed")
        {
            CPGain.text = "+ 20";
            cp += 20;
            CPText.text = cp.ToString();
            int totaldamage = Stark.getDamage() * 2 - Enemy.getDR();
            if (atkBoosted > 0)
                totaldamage *= 2;
            InfoBox.text = "Stark slashes extremely quickly for " + totaldamage + " damage! Act again.";
            EnemyDamageTaken.text = "-" + totaldamage;
            Enemy.hp -= totaldamage;
            EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
            StarkAnim.SetTrigger("SkillGS");
            StartCoroutine(StarkSkillGSAudio());
            yield return new WaitForSeconds(1);
            playerTurn = true;
            PlayerInfo.Instance.inventory.Remove(ItemList.options[ItemList.value].text);
            ItemList.options.RemoveAt(ItemList.value);
            ItemList.value = 0;
            action = 0;
            ItemList.RefreshShownValue();
            isAnimation = false;
            showUserUI();
            yield break;
        }
        if (ItemList.options[ItemList.value].text == "Rune: Bladesdance")
        {
            int totaldamage = Stark.getDamage() * 4;
            if (atkBoosted > 0)
                totaldamage *= 2;
            InfoBox.text = "Stark turns into a flash of steel and hits for " + totaldamage + " damage!";
            EnemyDamageTaken.text = "-" + totaldamage;
            Enemy.hp -= totaldamage;
            EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
            StarkAnim.SetTrigger("SkillSuper");
            StartCoroutine(StarkSkillSuperAudio());
            yield return new WaitForSeconds(2);
        }
        if (ItemList.options[ItemList.value].text.Contains("Rune: Explosive Stab"))
        {
            int totaldamage = (int)(Enemy.maxHp * .40) - Enemy.getDR();
            InfoBox.text = "Stark puts all of his strength into his stab and deals " + totaldamage + " damage!";
            EnemyDamageTaken.text = "-" + totaldamage;
            Enemy.hp -= totaldamage;
            EnemyHP.text = Enemy.hp + "/" + Enemy.maxHp;
            StarkAnim.SetTrigger("SkillGS");
            StartCoroutine(StarkSkillGSAudio());
            yield return new WaitForSeconds(1);
        }
        if (ItemList.options[ItemList.value].text == "Instant Kill")
        {
            Enemy.hp = -999;
            yield return new WaitForSeconds(1);
        }
        PlayerInfo.Instance.inventory.Remove(ItemList.options[ItemList.value].text);
        ItemList.options.RemoveAt(ItemList.value);
        ItemList.value = 0;
        action = 0;
        ItemList.RefreshShownValue();
        isAnimation = false;
        yield break;
    }

    public IEnumerator enemyAttack()
    {
        if (PlayerInfo.Instance.progress == 2 && lionelCounter < 2)
        {
            InfoBox.text = "Lionel prepares an extremely powerful blow...";
            lionelCounter++;
            if (guarding)
            {
                CPGain.text = "+ 15";
                cp +=  15;
            }
        }
        else
        {
            if(lionelCounter == 2)
            {
                EnemyAnim.SetTrigger("Lionel Attack");
                    StartCoroutine(LionelGrandCrossAudio());
                yield return new WaitForSeconds(1.75f);
                if (PlayerInfo.Instance.difficulty == 3)
                    lionelCounter = 1;
                else
                    lionelCounter = 0;
            }
            if (PlayerInfo.Instance.progress == 0)
                StartCoroutine(EnemyAttackAudio(false));
            if (PlayerInfo.Instance.progress == 1)
                StartCoroutine(EnemyAttackAudio(true));
            EnemyAnim.SetTrigger("Kyra Attack");
            EnemyAnim.SetTrigger("Vayne Attack");
            int originaldamage = Enemy.getDamage() + rand.Next(0, Enemy.getDamage() / 2) - Stark.getDR();
            int totaldamage = originaldamage;
            if (guarding)
                totaldamage /= 2;
            if (debilitated)
                totaldamage /= 2;
            if (tecBoosted)
                totaldamage /= 4;
            if(totaldamage < 0)
            {
                totaldamage = 0;
            }
            if (PlayerInfo.Instance.progress == 0)
                InfoBox.text = "Kyra deals " + totaldamage + " to Stark!";
            if (PlayerInfo.Instance.progress == 1)
                InfoBox.text = "Vayne deals " + totaldamage + " to Stark!";
            if (PlayerInfo.Instance.progress == 2)
                InfoBox.text = "Lionel deals " + totaldamage + " with his Grand Cross to Stark!";
            Stark.hp -= totaldamage;
            StarkDamageTaken.text = "-" + totaldamage;
            StarkHP.text = Stark.hp + "/" + Stark.maxHp;
            if (guarding)
            {
                CPGain.text = "+" + (originaldamage + 15);
                cp += originaldamage + 15;
            }
            else
            {
                CPGain.text = "+" + originaldamage / 2;
                cp += originaldamage / 2;
            }
            CPText.text = cp.ToString();
        }
        if (debilitatedTurns == 3)
        {
            debilitated = false;
            debilitatedTurns = 0;
        }
        else if (debilitated == true)
        {
            debilitatedTurns++;
        }
        guarding = false;
        tecBoosted = false;
        if (atkBoosted > 0)
            atkBoosted--;
        yield return new WaitForSeconds(1);

        showUserUI();
        isAnimation = false;
    }

    public void showUserUI()
    {
        if (!endFight)
        {
            Attack.interactable = true;
            Guard.interactable = true;
            if (ItemList.options.Count != 0)
                ItemList.enabled = true;
            if (cp > 19)
                PowerStrike.interactable = true;
            else
                PowerStrike.interactable = false;
            if (cp > 49)
                DebilitatingSlash.interactable = true;
            else
                DebilitatingSlash.interactable = false;
            if (cp > 69)
                Godspeed.interactable = true;
            else
                Godspeed.interactable = false;
            if (cp > 99)
                Bladesdance.interactable = true;
            else
                Bladesdance.interactable = false;
            if (PlayerInfo.Instance.inventory.Count != 0)
            {
                UseItem.interactable = true;
            }
            else
            {
                UseItem.interactable = false;
            }
        }
    }

    public IEnumerator starkGuard()
    {
        yield return new WaitForSeconds(1);
        action = 0;
        isAnimation = false;
    }

    public void disableUserUI()
    {
        Attack.interactable = false;
        Guard.interactable = false;
        PowerStrike.interactable = false;
        DebilitatingSlash.interactable = false;
        Godspeed.interactable = false;
        Bladesdance.interactable = false;
        UseItem.interactable = false;
        ItemList.enabled = false;
    }

    public void itemSelectTip()
    {
        if (ItemList.options.Count != 0 && !endFight)
        {
            if (ItemList.options[ItemList.value].text == "Health Potion")
                InfoBox.text = "Heals a small percentage of your max health";
            if (ItemList.options[ItemList.value].text == "Chronos Potion")
                InfoBox.text = "Grants you a bit of CP";
            if (ItemList.options[ItemList.value].text == "Super Potion")
                InfoBox.text = "Restores a lot of health";
            if (ItemList.options[ItemList.value].text == "Super Chronos Potion")
                InfoBox.text = "Energizes you with a ton of CP";
            if (ItemList.options[ItemList.value].text == "Attack Booster")
                InfoBox.text = "Doubles all damage dealt on your next turn (Does not apply to Explosive Stab)";
            if (ItemList.options[ItemList.value].text == "Tec Booster")
                InfoBox.text = "Lowers all damage taken by 75% until your next turn";
            if (ItemList.options[ItemList.value].text == "Rune: Godspeed")
                InfoBox.text = "Attack using Godspeed at no cost of CP";
            if (ItemList.options[ItemList.value].text == "Rune: Explosive Stab")
                InfoBox.text = "Attack with furious energy that deals 40% damage based on max health";
            if (ItemList.options[ItemList.value].text == "Rune: Bladesdance")
                InfoBox.text = "Attack using Bladesdance at no cost of CP";
            if (ItemList.options[ItemList.value].text == "Instant Kill")
                InfoBox.text = "Instantly kill the enemy";
        }
    }

    public IEnumerator StarkSkillAudio()
    {
        SlashSound.PlayDelayed(.3f);
        SlashSound2.PlayDelayed(.4f);
        yield break;
    }

    public IEnumerator StarkSkillGSAudio()
    {
        SlashSound.PlayDelayed(.15f);
        yield break;
    }

    public IEnumerator StarkSkillSuperAudio()
    {
        SlashSound.PlayDelayed(.3f);
        SlashSound2.PlayDelayed(.4f);
        SlashSound3.PlayDelayed(.6f);
        SlashSound4.PlayDelayed(.8f);
        SlashSound5.PlayDelayed(.1f);
        yield break;
    }

    public IEnumerator EnemyAttackAudio(bool x)
    {  
        if(!x)
            SlashSound.PlayDelayed(.1f);
        else
            BowSound.PlayDelayed(.25f);
        yield break;
    }

    public IEnumerator LionelGrandCrossAudio()
    {
        SlashSound.PlayDelayed(.15f);
        SlashSound2.PlayDelayed(.3f);
        SlashSound3.PlayDelayed(.5f);
        SlashSound4.PlayDelayed(.6f);
        SlashSound5.PlayDelayed(.9f);
        SlashSound6.PlayDelayed(1.1f);
        SlashSound7.PlayDelayed(1.3f);
        yield break;
    }

    public void InfoBoxPowerStrike()
    {
        if(PowerStrike.interactable == true)
            InfoBox.text = "Costs 20CP, Deals increased damage to your enemy by striking harder";
    }

    public void InfoBoxDebilitatingSlash()
    {
        if (DebilitatingSlash.interactable == true)
            InfoBox.text = "Costs 50CP, Deals additional damage and lower your enemy's damage for 3 turns";
    }

    public void InfoBoxGodspeed()
    {
        if (Godspeed.interactable == true)
            InfoBox.text = "Costs 70CP, Deals high damage and gain another action this turn";
    }

    public void InfoBoxBladesdance()
    {
        if (Bladesdance.interactable == true)
            InfoBox.text = "Costs 100CP, Deals massive damage to your enemy ignoring defense";
    }

    public void InfoBoxAttack()
    {
        if (Attack.interactable == true)
            InfoBox.text = "Deal damage and gain 15 CP";
    }

    public void InfoBoxGuard()
    {
        if (Guard.interactable == true)
            InfoBox.text = "Gain double CP from blocked damage, 15 additional CP, and reduce damage taken by half";
    }

    public void InfoBoxUseItem()
    {
        if (UseItem.interactable == true)
            InfoBox.text = "Uses the selected item above";
    }

    public void ClearInfoBox()
    {
        if(Attack.interactable == true)
            InfoBox.text = "";
    }

    public IEnumerator endBattle()
    {

        if(Stark.hp < 1)
        {
            StarkAnim.SetTrigger("Dead");
        }
        if (Enemy.hp < 1)
        {
            EnemyAnim.SetTrigger("Kyra Dead");
            EnemyAnim.SetTrigger("Vayne Dead");
            EnemyAnim.SetTrigger("Lionel Dead");
        }

        yield return new WaitForSeconds(1);

        InfoBox.text = "Battle Ended";

        yield return new WaitForSeconds(2);

        if (Stark.hp < 1)
        {
            InfoBox.text = "You died...";
            yield return new WaitForSeconds(2);
            InfoBox.text = "Reversing time...";
            yield return new WaitForSeconds(2);
            PlayerInfo.Instance.stark = Stark;
            PlayerInfo.Instance.stark.hp = PlayerInfo.Instance.stark.maxHp;
            PlayerInfo.Instance.progress = 0;
            SceneManager.LoadScene("Continue");
            yield break;
        }

        if (Enemy.hp < 1 && PlayerInfo.Instance.progress == 0)
        {
            InfoBox.text = "You won!";
            yield return new WaitForSeconds(2);
            InfoBox.text = "You leveled up three times!";
            Stark.levelUp();
            Stark.levelUp();
            Stark.levelUp();
            PlayerInfo.Instance.stark = Stark;
            yield return new WaitForSeconds(2);
            PlayerInfo.Instance.progress += 1;
            SceneManager.LoadScene("Ingame");
            yield break;
        }

        if (Enemy.hp < 1 && PlayerInfo.Instance.progress == 1)
        {
            InfoBox.text = "You won!";
            yield return new WaitForSeconds(3);
            InfoBox.text = "You leveled up five times!";
            Stark.levelUp();
            Stark.levelUp();
            Stark.levelUp();
            Stark.levelUp();
            Stark.levelUp();
            PlayerInfo.Instance.stark = Stark;
            yield return new WaitForSeconds(3);
            PlayerInfo.Instance.progress += 1;
            SceneManager.LoadScene("Ingame");
            yield break;
        }

        if (Enemy.hp < 1 && PlayerInfo.Instance.progress == 2)
        {
            InfoBox.text = "You won!";
            yield return new WaitForSeconds(3);
            InfoBox.text = "Congratulations on becoming a Grand Duelist!";
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("Credits");
        }
    }
}
