﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_MainMenu : MonoBehaviour
{
    public Button Button_StartGame;
    public Button Button_ExitGame;
    public GameObject[] set1;
    public GameObject[] set2;
    public Animator FadeToBlack;
    public Animator Music;

    void Awake()
    {
        if(PlayerInfo.Instance != null)
        {
            PlayerInfo.DestroyThis();
        }
        set1 = GameObject.FindGameObjectsWithTag("MenuSet1");
        set2 = GameObject.FindGameObjectsWithTag("MenuSet2");
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
    }

    public void difficultySelect()
    {
        foreach (GameObject x in set2)
            x.SetActive(true);
        foreach (GameObject x in set1)
            x.SetActive(false);
    }

    public void startGameEZ()
    {
        PlayerInfo.Instance.difficulty = 1;
        StartCoroutine(TransitionToGame());
    }

    public void startGameNM()
    {
        PlayerInfo.Instance.difficulty = 2;
        StartCoroutine(TransitionToGame());
    }

    public void startGameHD()
    {
        PlayerInfo.Instance.difficulty = 3;
        StartCoroutine(TransitionToGame());
    }

    public void back()
    {
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
    }


    public void quit()
    {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }

    public IEnumerator TransitionToGame()
    {
        foreach (GameObject x in set2)
        {
            x.GetComponent<Button>().interactable = false;
        }
        FadeToBlack.SetTrigger("Fade");
        Music.SetTrigger("Fade");
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Instructions");
    }
}
