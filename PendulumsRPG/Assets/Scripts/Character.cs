﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    public System.Random rand = new System.Random();
    public int level;
    public int hp;
    public int maxHp;
    public Dictionary<string, int> stats = new Dictionary<string, int>();

    public Character(int level, int maxhp, int atk, int tec)
    {
        this.level = level;
        this.hp = maxhp;
        this.maxHp = maxhp;
        this.stats.Add("atk", atk); //Primary - Physical attack strength.
        this.stats.Add("tec", tec); //Primary - Reduces incoming damage.
    }

    public void levelUp()
    {
        level++;
        this.maxHp += rand.Next(3, 6);
        this.hp += 3;
        this.stats["atk"] += rand.Next(2, 4);
        this.stats["tec"] += rand.Next(1, 3);

        if (hp > maxHp)
            hp = maxHp;
    }

    public int getDamage()
    {
        return stats["atk"] - (stats["atk"] / 4) + level;
    }

    public int getDR()
    {
        return (stats["tec"]+level) / 3;
    }
}
